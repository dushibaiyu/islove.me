@file:JvmName("Main")

package me.islove.home
import io.netty.util.internal.logging.Log4J2LoggerFactory
import io.netty.util.internal.logging.InternalLoggerFactory
import io.vertx.core.Vertx

import io.vertx.core.VertxOptions
import io.vertx.core.http.HttpServerOptions

fun main(args : Array<String>)
{
    val option = parseArgs(args)
    // set Log Config
    System.setProperty("log4j.configurationFile",option.getString("logConfigFile"))
    System.setProperty("vertx.logger-delegate-factory-class-name","io.vertx.core.logging.Log4j2LogDelegateFactory")
    InternalLoggerFactory.setDefaultFactory(Log4J2LoggerFactory.INSTANCE)

    println("The config version is ${option.getString("version")}")
    val vertxOption = VertxOptions(option.getJsonObject("vertx"));
    val vertx = Vertx.vertx(vertxOption)
    val httpOption = HttpServerOptions(option.getJsonObject("vertx-web"));
    val router = buildRouter(vertx)
    for(i in 0 .. vertxOption.eventLoopPoolSize){
        val server = vertx.createHttpServer(httpOption);
        server.requestHandler(router::accept).listen()
    }
    vertx.also {  }
}


