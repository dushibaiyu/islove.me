package me.islove.home.page

import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.core.logging.LoggerFactory
import java.nio.Buffer

internal val log = LoggerFactory.getLogger("me.islove.home.page.default")

fun Router.setPageRouter()
{
    this.route().failureHandler(::notFound) // set 404 handler
    this.post().handler(BodyHandler.create())
    this.get("/").handler{ event: RoutingContext ->
//        val log = LoggerFactory.getLogger("me.islove.test")
        log.debug("get ${event.request().query()}")
        event.response().end("Hello World")
    }


}
// log 是跟据前面包名做一层判断的， 如果me.islove设置为error， 那么下面的这句也是显示不出来的。
//val log2 = LoggerFactory.getLogger("me.islove.2")
//log2.debug("get ${event.request().query()}")