 
CREATE TABLE public.articles (
	id serial8 NOT NULL,
	title varchar(512) NOT NULL,
	writer varchar(64) NULL,
	body text NOT NULL,
	"time" varchar(64) NULL,
	"insertTime" timestamp NOT NULL DEFAULT now(),
	CONSTRAINT articles_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
create
    index articles_title_idx on
    articles
        using btree(title) ;

CREATE OR REPLACE VIEW public.articles_count AS
 SELECT count(articles.id) AS id_count,
    max(articles.id) AS id_max,
    min(articles.id) AS id_min
   FROM articles;
