package me.islove.home

import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import me.islove.home.page.setPageRouter

fun buildRouter(vertx : Vertx) : Router
{
    val router = Router.router(vertx);
    router.route().handler(BodyHandler.create())
    router.setPageRouter()
    return router;
}