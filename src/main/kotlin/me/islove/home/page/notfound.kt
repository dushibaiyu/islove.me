package me.islove.home.page

import io.vertx.ext.web.RoutingContext

fun notFound(context : RoutingContext){
    context.response().setStatusCode(404).end("can not found the page!");
}